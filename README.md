# Universal Widget

This project is small boilerplate code for a generic iOS 14 widget that downloads a configured library from GitLab (if not locally cached yet) and calls `.createWidget()` in that library. The library itself determines the specific functionality of the widget.

## Thanks

Thanks to [Simon](http://simonbs.dk/) for his incredibly fantastic iOS app "[Scriptable](http://scriptable.app/)"!

## Installation

- copy the `UniversalWidget.js` code into your Scriptable app
    - save [this iOS Shortcut](https://www.icloud.com/shortcuts/8cffed427b694e7d99285b771e5e3e36) to your Shortcuts app
    - go to the [raw file view](https://gitlab.com/sillium-scriptable-projects/universal-scriptable-widget/-/raw/master/UniversalWidget.js) on your iOS device
    - run the iOS Shortcut "Save to Scriptable"
- rename it to the desired name of your widget
- configure the library to be used
- run it

The script will create a subdirectory in your Scriptable documents folder. The subdirectory is named as your script itself but without the `.js` file extension. Inside that folder is the downloaded library file.

## Configuration

An example configuration could be:

```js
const libraryInfo = {
    name: 'HelloWorldWidgetLibrary',
    version: 'master',
    gitlabProject: 'https://gitlab.com/sillium-scriptable-projects/universal-scriptable-widget-libraries'
}
```

and would use <https://gitlab.com/sillium-scriptable-projects/universal-scriptable-widget-libraries/-/blob/master/HelloWorldWidgetLibrary.js> as the library.

## Advantages

- small widget code for the user to copy and paste into Scriptable
- library code can be maintained in GitLab
- widget can be configured to use
    - the `master`-branch of the library and will always show the updated implementation
    - a tag of the library and will not be affected by its further development
- widget code can easily be duplicated in the Scriptable app to configure another library and fullfil a different purpose
