// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: pink; icon-glyph: exchange-alt;
var CONFIG = {
    rangePerPercent: 1.9,
    batteryCapacity: 28,
    car: "hyundaiioniq",
    debug: false
}

const log = CONFIG.debug ? console.log.bind(console) : function () { };

// configure the library
const LIBRARY_INFO = {
    name: 'EVNotifyLibrary',
    version: 'master',
    gitlabProject: 'https://gitlab.com/sillium-scriptable-projects/universal-scriptable-widget-libraries',
    forceDownload: CONFIG.debug
}

if (args.widgetParameter == null || args.widgetParameter.length == 0 || args.widgetParameter.split(';').length != 2) {
    console.log("Error: Widget configuration missing")
} else {
    CONFIG.akey = args.widgetParameter.split(';')[0]
    CONFIG.token = args.widgetParameter.split(';')[1]
}

// download and import library
let library = importModule(await downloadLibrary(LIBRARY_INFO))

log(JSON.stringify(CONFIG, null, 2))

// create the widget
const widget = await library.createWidget(CONFIG)

// preview the widget
if (!config.runsInWidget) {
    await widget.presentSmall()
}

Script.setWidget(widget)
Script.complete()

/**
 * - creates directory for library if not existing
 * - downloads library file if not existing or forced
 * - returns relative path to library module
 * @param {{name: string, version: string, gitlabProject: string, forceDownload: bool}} library 
 */
async function downloadLibrary(library) {
    let fm = FileManager.local()

    let scriptPath = module.filename
    let libraryDir = scriptPath.replace(fm.fileName(scriptPath, true), fm.fileName(scriptPath, false))

    if (fm.fileExists(libraryDir) && !fm.isDirectory(libraryDir)) {
        fm.remove(libraryDir)
    }
    if (!fm.fileExists(libraryDir)) {
        fm.createDirectory(libraryDir)
    }
    let libraryFilename = library.name + '_' + library.version + '.js'
    let path = fm.joinPath(libraryDir, libraryFilename)

    let forceDownload = (library.forceDownload) ? library.forceDownload : false
    if (fm.fileExists(path) && !forceDownload) {
        log("Not downloading library file")
    } else {
        let r = Math.random().toString(36).substring(7);
        let libraryUrl = library.gitlabProject + '/-/raw/' + library.version + '/' + library.name + '.js?random=' + r
        log("Downloading library file '" + libraryUrl + "' to '" + path + "'")
        const req = new Request(libraryUrl)
        let libraryFile = await req.load()
        fm.write(path, libraryFile)
    }
    
    log(fm.fileName(scriptPath, false) + '/' + libraryFilename)

    return fm.fileName(scriptPath, false) + '/' + libraryFilename
}